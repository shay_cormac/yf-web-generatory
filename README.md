## 安装
```
$ npm install -g git@gitee.com:shay_cormac/yf-web-generatory.git
```

## 创建项目
```
$ yf-web create <项目名称> 
```

## 创建新页面
使用创建新页面的的命令时，此命令会自动创建页面文件（index.tsx 、 index.less）、type类型约束文件、models文件
```
$ yf-web newPage <新页面名称>
```